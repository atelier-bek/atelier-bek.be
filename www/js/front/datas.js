// -------------------- 

// # INIT

window.onload = datasGet();

// -------------------- 

// # DATAS VARIABLES
function datasVariables(){
    console.log("function : datasVariables");
   
}

// -------------------- 

// # DATAS EVENTS LISTENERS
function datasEventsListeners() {
    console.log("function : datasEventsListeners");
}

// -------------------- 

// # DATAS GET 
function datasGet(){
    console.log("function : datasGet");
    
    datasDiv = document.getElementById("datasDiv");
    
    // ## REQUEST
    var request = $.ajax({
	url: "php/front/datasGet.php",
	type: "GET",
    });

    // ## DONE
    request.done(function(content) {
	datasDiv.innerHTML = content;
	
	imageArr = document.getElementsByClassName("image");

	// imageArr -> hover -> function : seeImage
	for (i = 0; i < imageArr.length; i ++) {
	    imageArr[i].addEventListener("click", seeImage);
	}

	datasSort();
    });
}

// -------------------- 

function datasSort(){
    console.log("function : datasSort");

    datasDiv = document.getElementById("datasDiv");
    
    var itemArr = document.getElementsByClassName("item");
    console.log(itemArr);

    var sortArr = [];
    for (var i = 0; i < itemArr.length; i++) {

	var sendDateTimeStr = itemArr[i].className.split(" ")[0];
	console.log(sendDateTimeStr);

	var id = itemArr[i].id;
	console.log(id);

	sortArr.push([sendDateTimeStr, itemArr[i]]);
	}
    
    console.log(sortArr);
    sortArr.sort();
    sortArr.reverse();
    console.log(sortArr);
    
    for (var i = 0; i < sortArr.length; i++) {
	datasDiv.appendChild(sortArr[i][1]);
    }
}

// -------------------- 

function seeImage(){
    console.log("function : seeImage");

    if ($(this).hasClass("resized")) {
	this.classList.add("sources");
	this.classList.remove("resized");
	var oldSrc = this.src;
	var newSrc = oldSrc.replace("resized", "sources"); 
	this.src = newSrc;
    } else {
	this.classList.add("resized");
	this.classList.remove("sources");
	var oldSrc = this.src;
	var newSrc = oldSrc.replace("sources", "resized"); 
	this.src = newSrc;
    }
}
