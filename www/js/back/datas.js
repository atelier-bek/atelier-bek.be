// -------------------- 

// # INIT

window.onload = datasVariables();
window.onload = datasGet();
window.onload = sendDivGet();


// -------------------- 

// # DATAS VARIABLES
function datasVariables(){
    console.log("function : datasVariables");
   
    rightDiv = document.getElementById("rightDiv");
    datasDiv = document.getElementById("datasDiv");
}

// -------------------- 

// # DATAS EVENTS LISTENERS
function datasEventsListeners() {
    console.log("function : datasEventsListeners");
}

// -------------------- 

// # SEND DIV GET 
function sendDivGet(){
    console.log("function : sendDivGet");
    
    // ## REQUEST
    var request = $.ajax({
	url: "php/back/sendDiv.php",
	type: "GET",
    });

    // ## DONE
    request.done(function(content) {
	rightDiv.innerHTML = content;

	sendBtn = document.getElementById("sendBtn");
	// sendBtn -> click -> function : datasPost
	sendBtn.addEventListener("click", datasPost);

	fileCleanBtn = document.getElementById("fileCleanBtn");
	// fileCleanBtn -> click -> function : fileClean
	fileCleanBtn.addEventListener("click", fileClean);
    });

}

// -------------------- 

// # EDIT DIV GET 
function editDivGet(){
    console.log("function : editDivGet");

   
    var id = this.parentNode.id;

    // ## DATA
    var dataObj = {};
    dataObj["id"] = id;
    
    // ## REQUEST
    var request = $.ajax({
	url: "php/back/editDiv.php",
	type: "POST",
	data: dataObj,
    });

    // ## DONE
    request.done(function(content) {
	rightDiv.innerHTML = content;

	sendBtn = document.getElementById("sendBtn");
	// sendBtn -> click -> function : datasPost
	sendBtn.addEventListener("click", datasPost);

	fileCleanBtn = document.getElementById("fileCleanBtn");
	// fileCleanBtn -> click -> function : fileClean
	fileCleanBtn.addEventListener("click", fileClean);
    });

}
// -------------------- 

// # REMOVE DATAS 
function removeDatas(){
    console.log("function : removeDatas");
   
    var id = this.parentNode.id;

    // ## DATA
    var dataObj = {};
    dataObj["id"] = id;
    
    // ## REQUEST
    var request = $.ajax({
	url: "php/back/removeDatas.php",
	type: "POST",
	data: dataObj,
    });

    // ## DONE
    request.done(function(content) {
	alert(content);
	datasGet();
    });

}
// -------------------- 

// # DATAS GET 
function datasGet(){
    console.log("function : datasGet");
    
    // ## REQUEST
    var request = $.ajax({
	url: "php/back/datasGet.php",
	type: "GET",
    });

    // ## DONE
    request.done(function(content) {
	datasDiv.innerHTML = content;
	
	editBtnArr = document.getElementsByClassName("editBtn");
	// editBtn    
	for (i = 0; i < editBtnArr.length; i++){
	    editBtnArr[i].addEventListener("click", editDivGet);
	}
	removeBtnArr = document.getElementsByClassName("removeBtn");
	// removeBtn    
	for (i = 0; i < removeBtnArr.length; i++){
	    removeBtnArr[i].addEventListener("click", removeDatas);
	}

	imageArr = document.getElementsByClassName("image");

	// imageArr -> hover -> function : seeImage
	for (i = 0; i < imageArr.length; i ++) {
	    imageArr[i].addEventListener("click", seeImage);
	}

	datasSort();

    });
}

// -------------------- 

// # DATAS POST 
function datasPost(){
    console.log("function : datasPost");

    alert("Patienter");
    
    // ## DATAS
    // create form data and gets datas
    var formData = new FormData();
    if (document.getElementById("id")) {
        var id = document.getElementById("id").innerHTML;	
    } else {
        console.log("no id");
        var id="";
	console.log(id);
    }
    var author = [];
    $("input:checkbox[name=author]:checked").each(function(){
        author.push($(this).val());
    });
    var author = author.toString();
    console.log(author);
    category = [];
    $("input:checkbox[name=category]:checked").each(function(){
        category.push($(this).val());
    });
    var category = category.toString();
    console.log(category);
    var text = document.getElementById("text").value;
    console.log(text);
    var file = document.getElementById("file").files;
    console.log(file);
    var date = document.getElementById("date").value;
    console.log(date);
    var time = document.getElementById("time").value;
    console.log(time);
    
    // append datas to form datas and file to form file
    formData.append('data[]', id); 
    formData.append('data[]', author); 
    formData.append('data[]', category); 
    formData.append('data[]', text); 
    formData.append('data[]', date); 
    formData.append('data[]', time); 
    for (i = 0; i < file.length; i ++) {
    	formData.append('file[]', file[i]);
    }
   
   // ## REQUEST
    var request = $.ajax({
	url: "php/back/datasPost.php",
	type: "POST",
	processData: false,
	contentType: false,
	data: formData 
    });

    // ## DONE
    request.done(function(content) {
	if (content == "Votre post a bien été envoyé. Merci.") {
	    sendDivGet();
	}
	alert(content);
	datasGet();
    });
}

// -------------------- 

// # FILE CLEAN INPUT

function fileClean(){
    console.log("function : fileClean");

    $("#file").val('');
}

// -------------------- 

function datasSort(){
    console.log("function : datasSort");

    datasDiv = document.getElementById("datasDiv");
    
    var itemArr = document.getElementsByClassName("item");
    console.log(itemArr);

    var sortArr = [];
    for (var i = 0; i < itemArr.length; i++) {

	var sendDateTimeStr = itemArr[i].className.split(" ")[0];
	console.log(sendDateTimeStr);

	var id = itemArr[i].id;
	console.log(id);

	sortArr.push([sendDateTimeStr, itemArr[i]]);
	}
    
    console.log(sortArr);
    sortArr.sort();
    sortArr.reverse();
    console.log(sortArr);
    
    for (var i = 0; i < sortArr.length; i++) {
	datasDiv.appendChild(sortArr[i][1]);
    }
}

// -------------------- 

function seeImage(){
    console.log("function : seeImage");

    if ($(this).hasClass("resized")) {
	this.classList.add("sources");
	this.classList.remove("resized");
	var oldSrc = this.src;
	var newSrc = oldSrc.replace("resized", "sources"); 
	this.src = newSrc;
    } else {
	this.classList.add("resized");
	this.classList.remove("sources");
	var oldSrc = this.src;
	var newSrc = oldSrc.replace("sources", "resized"); 
	this.src = newSrc;
    }
}
