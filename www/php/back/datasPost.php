<?php

    include "../../inc/variables.php";

    # get datas from post
    $idValue = $_POST['data'][0];
    $authorValue = $_POST['data'][1];    
    $categoryValue = $_POST['data'][2];    
    $textValue = $_POST['data'][3];    
    $dateValue = $_POST['data'][4];    
    $timeValue = $_POST['data'][5];

    # get files from post
    $fileCount = count($_FILES['file']['name']);

    # check date and time format
    $reg_date = '/\b[0-9][0-9][0-9][0-9]-(0[1-9]|1[0-2])-([0-2][0-9]|3[0-1])\b/';
    $reg_time = '/\b([0-1][0-9]|2[0-3])-[0-5][0-9]-[0-5][0-9]\b/';

    # check posted datas or files and error messages
    if (empty($authorValue)) {
	echo "Merci de choisir au moins un auteur.";
    } else if (empty($categoryValue)) {
	echo "Merci de choisir au moins une catégorie.";
    } else if (strlen($textValue) === 0 && $fileCount === 0  ) {
        echo "Vous ne pouvez pas envoyer de post vide. Merci d'écrire un texte ou d'ajouter un fichier.";
    } else if (!preg_match($reg_date, $dateValue) && !empty($dateValue)) {
	echo "Merci de choisir une date au format yy-mm-dd.";
    } else if (!preg_match($reg_time, $timeValue) && !empty($timeValue)) {
	echo "Merci de choisir une heure au format hh-mm-ss.";
    } else {
	
	# generate send date and time if empty
	if (empty($dateValue)) {
	    $dateValue = date('Y-m-d');
	}
	if (empty($timeValue)) {
	    $timeValue = date('H-i-s');
	}

	# check if id is empty (new post) of set (editing)
	if (strlen($idValue) == 0){
	    # get count
	    $lastId = file_get_contents($countFile);
	    $lastId = (int)$lastId;
	    $id = $lastId + 1;
	    file_put_contents($countFile, "");
	    file_put_contents($countFile, $id);

	    # create dirs
	    mkdir($datasDir . "/" . $id, 0777);
	    mkdir($datasDir . "/" . $id . "/" . "files", 0777);
	    mkdir($datasDir . "/" . $id . "/" . "images_sources", 0777);
	    mkdir($datasDir . "/" . $id . "/" . "images_resized", 0777);
	} else {
	    $id = $idValue;
	}

	# imagesResizer
	include "imagesResizer.php";
    
	# iterate trough files and move in corresponding files dir 
	for($i = 0; $i < $fileCount; $i++) {

	    # files names
	    $fileTmp_name = $_FILES['file']['tmp_name'][$i];
	    $fileName = $_FILES['file']['name'][$i];

	    # mime type	for images
	    $mime = mime_content_type($fileTmp_name);
	    $reg_image = '/image\/.*/';

	    # if image or else file 
	    if (preg_match($reg_image, $mime)) {
		
		# move into images_sources	    
		move_uploaded_file($fileTmp_name,  $datasDir . "/" . $id . "/" . "images_sources" . "/". $fileName);
		
		# resize and move into images_resized
		smart_resize_image($datasDir . "/" . $id . "/" . "images_sources" . "/". $fileName, null, 200 , 200, true , $datasDir . "/" . $id . "/" . "images_resized" . "/". $fileName , false , false ,100 );
	    } else {
		
		# mote to files
		move_uploaded_file($fileTmp_name,  $datasDir . "/" . $id . "/" . "files" . "/". $fileName);
	    }
	}

	# xml dom doc and params
	$xml = new DOMDocument('1.0'); # new dom doc
	$xml->formatOutput = true; # format output
	$xml->preserveWhiteSpace = false; # preserve whitespace

	# create element as root 
	$item = $xml->createElement("item");

	# create elements
	$date = $xml->createElement("date", $dateValue);
	$time = $xml->createElement("time", $timeValue);
	$author = $xml->createElement("author", $authorValue);
	$category = $xml->createElement("category", $categoryValue);
	$text = $xml->createElement("text", $textValue);
	
	# append elements to post 
	$item->appendChild($date);
	$item->appendChild($time);
	$item->appendChild($author);
	$item->appendChild($category);
	$item->appendChild($text);

	# append post to xml
	$xml->appendChild($item);
	
	# save xml
	$file = $datasDir . "/" . $id . "/" . "datas.xml";
	$xml->save($file);
	
	# message 
	echo "Votre post a bien été envoyé. Merci.";

    }
?>
