<div>
    <h1>Atelier Bek</h1>
    <p>Rue du ruisseau 15</p>
    <p>1080 Molenbeek-Saint-Jean</p>
    <p>contact@atelier-bek.be</p>
    <p>+ 32 419 60 09</p>
</div>

<!--
<div>
    L'Atelier Bek a la volonté de construire un environnement commun accueillant des pratiques pluridisciplinnaires. L'équipe actuelle se compose de designers (graphique, textile), illustrateurs, artistes plasticiens et artisans. Propice à ancrer ces pratiques dans une socialité, à la  fois interne à notre environnement et ouverte sur l’extérieur, l'atelier tend à une autonomie d'action et une ouverture à un ensemble d'initiatives. Celles-ci prennent forme autour de projets commandités ou auto-initiés, collectifs ou individuels. L'atelier permet également l’organisation d’événements et d’activités culturelles tels que des workshops, conférences, résidences, expositions, performances… En  perpetuelle mutation, tant sur l'organisation spatiale que le roulement des personnes de l'atelier Bek, nous sommes convaincus que l'échange, le partage et la mutualisation des savoirs et expériences sauront modeler nos métiers avec effervescence et enthousiasme. 
</div>
-->

<div>
    <?php
        foreach ($linkArr as $link => $www) {
    	echo "<p><a href=\"$www\">$link</a></p>";
        }
    ?>
</div>

<div>
    <?php
        foreach ($membreArr as $membre => $www) {
    	echo "<p><a href=\"$www\">$membre</a></p>";
        }
    ?>
</div>

