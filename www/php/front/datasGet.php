<?php

    # get all items from datas dir
    include "../../inc/variables.php"; 
    $itemArr = glob($datasDir . "/*",GLOB_ONLYDIR);
    $reversedItemArr = array_reverse($itemArr);

    # iterate trough each items    
    foreach ($reversedItemArr as $item) {

	# get id
	$id = basename($item);	

	# load datas.xml
	$xml = simplexml_load_file($item . "/datas.xml");
	
	# get date time
	$sendDateStr = $xml->date;
	$sendTimeStr = $xml->time;
	$sendDateTimeStr = $sendDateStr . "_" . $sendTimeStr;

	# create a div
	echo "<div id=\"" . $id . "\" class=\"" . $sendDateTimeStr . " item\">";

	    # get text and regex url 
	    $text = $xml->text;
	    $regex = preg_replace(
	      '#((https?|ftp)://(\S*?\.\S*?))([\s)\[\]{},;"\':<]|\.\s|$)#i',
	      "<a href=\"$1\" target=\"_blank\">$3</a>$4",
	      $text
	    );
	    echo "<p class=\"text\">" . $regex . "</p>";

	    # get resized images and display
	    echo "<div class=\"images\">";
	    $images = glob($item . "/images_resized/*");
	    foreach ($images as $image) {
		$imageName = basename($image);
		$imageExt = pathinfo($image, PATHINFO_EXTENSION);
		$imagePath = substr($image,6); #remove ../../ (index.php is 2 dir before datasGet.php)
		echo "<img class=\"image resized\" src=\"" . $imagePath . "\">"; 
	    }	    
	    echo "</div>";
	    
	    # get files
	    echo "<div class=\"files\">";
	    $files = glob($item . "/files/*");
	    foreach ($files as $file) {
		    $fileName = basename($file);
		    $fileExt = pathinfo($file, PATHINFO_EXTENSION);
		    $filePath = substr($file,6); #remove ../../ / (back.php is 2 dir before datasGet.php)
		    if (in_array($fileExt, $imagesExtArr)) {
		    } else {
			echo "<p><a target=\"_blank\" class=\"file\" href=\"" . $filePath . "\">" . $fileName . "</a></p>"; 
		    }

		}
	    echo "</div>";

	    # other datas
	    $categoryStr = $xml->category;
	    $categoryArr = explode(",", $categoryStr);
	    $authorStr = $xml->author;
	    $authorArr = explode(",", $authorStr);

	    $sendDateTimeStr = $sendDateStr . "_" . $sendTimeStr;
	    $convertDateStr = DateTime::createFromFormat('Y-m-d', $sendDateStr)->format('d-F-Y');
	    $sendDateArr = explode("-", $convertDateStr);
	    $sendTimeArr = explode("-", $sendTimeStr);

	    echo "<p class=\"other\">";
		echo "<span class=\"sendDateTime\">" . $sendDateArr[0] . " " . $sendDateArr[1] . " " . $sendDateArr[2] . " à " . $sendTimeArr[0] . ":" . $sendTimeArr[1] . ":" . $sendTimeArr[2] . "</span>";
		echo " dans";
		foreach ($categoryArr as $category) {
		    echo " <span data-filterType=\"category\" class=\"category\">$category</span>";
		}
		echo " par";
		foreach ($authorArr as $author) {
		    echo " <span data-filterType=\"author\" class=\"author\">$author</span>";
		}
	    echo "</p>";

	echo "</div>";
    }
?>
