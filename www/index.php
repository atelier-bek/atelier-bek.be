<html>
    
    <head>

	<!-- inc --> 
	<?php include "inc/meta.php"; ?>
	<?php include "inc/title.php"; ?>

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet/less" type="text/css" href="css/columns.less" />
	<link rel="stylesheet/less" type="text/css" href="css/styles.less" />
	<link rel="stylesheet/less" type="text/css" href="css/front.less" />
	<link rel="stylesheet/less" type="text/css" href="css/datasDiv.less" />

	<!-- fonts -->
	<link rel="stylesheet" type="text/css" href="fonts/fonts.css" />
	
	<!-- lib -->
	<script src="lib/less.min.js" type="text/javascript"></script>
	<script src="lib/jquery-2.1.4.min.js" type="text/javascript"></script>

    </head>

    <body>
	
	<?php include "inc/variables.php"; ?>

	<div class="p-width-2" id="datasDiv">
	</div>

	<div class="p-width-1" id="naviDiv">

	    <?php include "php/front/presentationText.php"; ?>

	</div>
	
	<div class="p-width-1" id="infoDiv">
	    <?php include "php/front/fixeDiv.php"; ?>
	</div>


    </body>

<!-- js -->
<script type="text/javascript" src="js/front/datas.js"></script>

</html>


